import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main(args:Array<String>) = runBlocking {
   launch {
       println("World")
       delay(1000L)
   }
   println("Hello, ")
   delay(2000L)
}

