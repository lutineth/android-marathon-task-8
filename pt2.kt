import kotlinx.coroutines.*

suspend fun first(num: Int): Int{
    delay(500)
    return num
}

suspend fun second(num: Int): Int{
    delay(1000)
    return num
}

suspend fun main()= coroutineScope{
    val startTime1 = System.currentTimeMillis()
    val serialNum1 = first(13)
    val serialNum2 = second(27)
    val serialSum = serialNum1 + serialNum2
    val endTime1 = System.currentTimeMillis() - startTime1
    println("Serial Sum = $serialSum; Serial Time = $endTime1")
   
    val startTime2 = System.currentTimeMillis()
    val asyncNum1 = async { first(13) }
    val asyncNum2 = async { second(27) }
    val asyncSum = asyncNum1.await() + asyncNum2.await()
    val endTime2 = System.currentTimeMillis() - startTime2
    println("Async Sum = $asyncSum; Async Time = $endTime2")
    
    println("В случае асинхронного вызова функции выполняются параллельно, а в случае последовательного")
    println("друг за другом, что объясняет разницу во времени работы в пользу асинхронного вызова.")
}
